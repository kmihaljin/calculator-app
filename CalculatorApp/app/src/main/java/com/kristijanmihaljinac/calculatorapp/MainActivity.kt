package com.kristijanmihaljinac.calculatorapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var lastNumeric : Boolean = false
    var lastDot : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onDigit(view: View) {
         val btn = view as Button
         tvInput.append(btn.text)
         lastNumeric = true
    }

    fun onClear(view: View){
        tvInput.text = ""
        lastNumeric = false
        lastDot = false
    }

    fun onDecimalPoint(view: View){
        if(lastNumeric && !lastDot){
            tvInput.append(getString(R.string.dot))
            lastNumeric = false
            lastDot = true
        }
    }

    fun onEqual(view: View){
        if(lastNumeric){
            var tvValue = tvInput.text.toString()
            var prefix = ""
            try {

                if(tvValue.startsWith("-"))
                {
                    prefix = "-"
                    tvValue = tvValue.substring(1)
                }

                if(tvValue.contains("-")){
                    val splitValue = tvValue.split("-")

                    var first = splitValue[0].toDouble()
                    var second = splitValue[1].toDouble()

                    if(prefix == "-"){
                        first *= -1
                    }

                    tvInput.text = removeZeroAfterDot("${first - second}")
                }
                else if(tvValue.contains("+")){

                    val splitValue = tvValue.split("+")

                    var first = splitValue[0].toDouble()
                    var second = splitValue[1].toDouble()

                    if(prefix == "-"){
                        first *= -1
                    }

                    tvInput.text = removeZeroAfterDot("${first + second}")

                }
                else if(tvValue.contains("*")){

                    val splitValue = tvValue.split("*")

                    var first = splitValue[0].toDouble()
                    var second = splitValue[1].toDouble()

                    if(prefix == "-"){
                        first *= -1
                    }

                    tvInput.text = removeZeroAfterDot("${first * second}")

                }
                else if(tvValue.contains("/")){

                    val splitValue = tvValue.split("/")

                    var first = splitValue[0].toDouble()
                    var second = splitValue[1].toDouble()

                    if(prefix == "-"){
                        first *= -1
                    }

                    if(second > 0){
                        tvInput.text = removeZeroAfterDot("${first / second}")
                    }
                    else {
                        tvInput.text = "Infinity"
                    }


                }
            }
            catch (e: ArithmeticException){
                e.printStackTrace()
            }
        }
    }

    private fun removeZeroAfterDot(result: String): String{
        var value = result

        if(result.contains(".0"))
            value = result.substring(0, result.length - 2)

        return  value
    }

    fun onOperator(view: View){
        if (lastNumeric && !isOperatorAdded(tvInput.text.toString())){
            tvInput.append((view as Button).text)
            lastDot = false
            lastNumeric = false
        }
    }

    private fun isOperatorAdded(value: String) : Boolean
    {
        return if (value.startsWith("-")) false else { value.contains("/") || value.contains("+") || value.contains("-") || value.contains("*")}
    }
}